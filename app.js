import SMSverification from "./services/SMSverification";
import promiseSynchronizer from "promise-synchronizer";
import randomatic from "randomatic";
import Btchot from "./services/btchot";
import BtchotSql from "./services/btchotSql";
import Dama2Service from "./services/dama2";
//randomize('Aa0!', 10);
//=> 'LV3u~BSGhw'

const config = require("config-lite")(__dirname);

const dama2Service = new Dama2Service(
  config.dama2UserInfo
);

const sMSverification = new SMSverification(
  config.SMSverification.uName,
  config.SMSverification.pWord,
  config.SMSverification.Developer
);

const btchot = new Btchot();
const btchotSql = new BtchotSql();

async function start() {
  //设置token
  let ressetToken = await promiseSynchronizer(sMSverification.setToken());
  //获取token
  let getTokenRes = sMSverification.getToken();
  let token = "";
  if (!getTokenRes.result) {
    console.log(getTokenRes.value);
  } else {
    token = getTokenRes.value;
    console.log(token);
    //获取项目手机号
    let getPhoneJson = {
      token: token,
      ItemId: config.ItemId,
      code: "utf8"
    };

    let getPhoneRes = await promiseSynchronizer(
      sMSverification.getPhone(getPhoneJson)
    );
    if (!getPhoneRes.result) {
      console.log("getPhone error: ", getPhoneRes.value);
    } else {
      console.log("ssss:",getPhoneRes.value);
      let Phone = getPhoneRes.value;
      let releasePhoneJson = {
        token: token,
        phoneList: `${Phone}-${config.ItemId}`
      }
      //设置cookie
      let setCookieRes = await promiseSynchronizer(
        btchot.setCookie()
      );
      if (!setCookieRes.result) {
        console.log("setCookie error: ", setCookieRes.value);
      } else {
        //验证手机号是否注册了
        let checkmobleRes = await promiseSynchronizer(
          btchot.checkmoble(Phone)
        );
        if (!checkmobleRes.result) {
          console.log("checkmoble error: ", checkmobleRes.value);
          //释放手机号
          let releasePhoneRes = await promiseSynchronizer(
            sMSverification.releasePhone(releasePhoneJson)
          );
        } else {
          if(checkmobleRes.value.status !== 1){
            console.log("checkmoble error: ", checkmobleRes.value);
            //释放手机号
            let releasePhoneRes = await promiseSynchronizer(
              sMSverification.releasePhone(releasePhoneJson)
            );
          }else{
            //手机号没有注册，接下来进行注册
              //获取验证码图片
              let verifyRes = await promiseSynchronizer(
                btchot.verify()
              );
              if (!verifyRes.result) {
                console.log("verify error: ", verifyRes.value);
                //释放手机号
                let releasePhoneRes = await promiseSynchronizer(
                  sMSverification.releasePhone(releasePhoneJson)
                );
              } else {
                //解析图片验证码
                let verifyPath = verifyRes.value;
                let ImageType = config.dama2ImageType;
                let dama2ServiceRes = await promiseSynchronizer(
                  dama2Service.d2File(verifyPath,ImageType)
                );
                if (!dama2ServiceRes.result) {
                  console.log("dama2Service error: ", dama2ServiceRes.value);
                  //释放手机号
                  let releasePhoneRes = await promiseSynchronizer(
                    sMSverification.releasePhone(releasePhoneJson)
                  );
                } else {
                  //请求发送短信
                  let verifyD2File = dama2ServiceRes.value;
                  let mobleRegRes = await promiseSynchronizer(
                    btchot.mobleReg(Phone,verifyD2File)
                  );
                  if (!mobleRegRes.result) {
                    console.log("mobleReg error: ", mobleRegRes.value);
                    //释放手机号
                    let releasePhoneRes = await promiseSynchronizer(
                      sMSverification.releasePhone(releasePhoneJson)
                    );
                  } else {
                    console.log("mobleReg: ", mobleRegRes.value);
                    //接收短信
                    let getMessageJson = {
                      token: token,
                      ItemId: config.ItemId,
                      Phone: Phone,
                      code: "utf8"
                    };
                    let interval = setInterval(async function() {
                      let getMessageRes = await promiseSynchronizer(sMSverification.getMessage(getMessageJson));
                      if(!getMessageRes.result){
                          console.log("getMessage error: ",getMessageRes.value);
                        
                      }else{
                        console.log("getMessage: ",getMessageRes);
                          let getVerificationRes = sMSverification.getVerification(getMessageRes.value);
                          if(!getVerificationRes.result){
                              console.log("getVerification error: ",getVerificationRes.value);
                          }else{
                              console.log('getVerification:   ',getVerificationRes.value);
                              //开始注册
                              let password = randomatic('Aa0', 13);
                              let invit = config.invit;
                              let regUpRes = await promiseSynchronizer(
                                btchot.regUp(Phone,getVerificationRes.value,password,invit)
                              );
                              if(!regUpRes.result){
                                console.log("regUp error: ",regUpRes.value);
                                //释放手机号
                                let releasePhoneRes = await promiseSynchronizer(
                                  sMSverification.releasePhone(releasePhoneJson)
                                );
                              }else{
                                //注册成功，完善信息
                                let username = randomatic('Aa0', 10);
                                let email = randomatic('0', 10)+'@qq.com';
                                let paypassword = randomatic('0', 6);
                                let paypasswordUpRes = await promiseSynchronizer(
                                  btchot.paypasswordUp(username,email,paypassword)
                                );
                                if(!paypasswordUpRes.result){
                                  console.log("paypasswordUp error: ",paypasswordUpRes.value);
                                  //释放手机号
                                  let releasePhoneRes = await promiseSynchronizer(
                                    sMSverification.releasePhone(releasePhoneJson)
                                  );
                                }else{
                                  //成功,写入数据库
                                  let regUserInfo = {
                                    Phone: Phone,
                                    password: password,
                                    invit: invit,
                                    username: username,
                                    email: email,
                                    paypassword: paypassword
                                  }
                                  console.log('regUserInfo: ',JSON.stringify(regUserInfo,null,2))
                                  //写入数据库
                                  let createbtchotRes = await promiseSynchronizer(
                                    btchotSql.createbtchot(regUserInfo)
                                  );
                                  if(!createbtchotRes.result){
                                    console.log("createbtchot error: ",createbtchotRes.value);
                                    //释放手机号
                                    let releasePhoneRes = await promiseSynchronizer(
                                      sMSverification.releasePhone(releasePhoneJson)
                                    );
                                  }else{
                                    console.log(createbtchotRes.value+'写入数据库成功');
                                    let releasePhoneRes = await promiseSynchronizer(
                                      sMSverification.releasePhone(releasePhoneJson)
                                    );
                                  }
                                  clearInterval(interval);
                                  //释放手机号
                                  
                                }
                              }
                          }
                      }
                    }, 5000);
                  }
                }
              }
          }
        }
      }
    }
  }
}
start();

