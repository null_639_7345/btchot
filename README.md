# btchot
批量自动注册彼热岛

## 1.分析注册流程
* 获取cookie
url：https://www.btchot.com/
method： get

* 验证手机号是否注册了
url：https://www.btchot.com/reg/check_moble.html
method： get
参数：
moble:18820227743
mobles:+86

* 获取图片验证码
https://www.btchot.com/ajax/verify.html?t=0.14478377591792269
get
T= Math.random()

* 请求发送短信
www.btchot.com/verify/moble_reg.html
post
参数：
moble:18820227743
mobles:+86
type:sms
verify: 验证码
token:

* 注册信息
Post
https://www.btchot.com/reg/reg_up.html
参数：
moble	18124773427
mobles	+86
moble_verify	253940
password	19970113520Xu
invit	EX991259


## 2.图片验证码破解
* 注册开发者账号和普通用户账号 http://www.dama2.com/
* 登录开发者账号创建软件
* 普通用户账号充值
* 确认图片二维码类型：http://wiki.dama2.com/index.php?n=ApiDoc.Pricedesc
* 配置config/default.js文件
```js
    dama2UserInfo: {
        "appID":"***",         // 开发者创建的软件的id
        "appKey":"***",     // 开发者创建的软件appKey
        "user":"***",   //普通使用者的账号
        "pwd":"***"    //普通使用者的密码
    },
    dama2ImageType: 42, //http://wiki.dama2.com/index.php?n=ApiDoc.Pricedesc
```

## 3.短信验证码破解
* 注册千万卡平台账号 http://www.yika66.com/index.aspx
* 充值
* 配置config/default.js文件
```js
    SMSverification: {
        uName: '***', 
        pWord: "***",
        Developer: "***"
    },
    ItemId: "***", //接收短信的项目id
```

## 4.数据库
* 新建clicklink数据库
* 新建btchot表
>表的列请参考：models/btchot.js文件

## 4.运行
```1sh
npm i
node index.js
```




