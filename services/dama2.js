import superagent from "superagent";
import Dama2 from "dama2";
import fs from "fs";

class Dama2Service {
  constructor(dama2UserInfo) {
    this.userInfo = dama2UserInfo;
    this.dama2 = new Dama2(
      dama2UserInfo.appID,
      dama2UserInfo.appKey,
      dama2UserInfo.user,
      dama2UserInfo.pwd
    );
  }

  /**
   * 
   * @param {*} filePath 
   * @param {*} ImageType 42 参考http://wiki.dama2.com/index.php?n=ApiDoc.Pricedesc
   */
  async d2File(filePath, ImageType) {
    let _this = this;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise((resolve, reject) => {
      if (!fs.existsSync(filePath)) {
        returnJson.result = false;
        returnJson.value = "图片验证失败，本地图片路径不存在";
        console.log(returnJson);
        reject(returnJson);
        return returnJson;
      } else {
        _this.dama2.login(function(error, auth) {
          if (error) {
            returnJson.result = false;
            returnJson.value = "解析图片验证码失败，dama2登录失败： " + error;
            console.log(returnJson);
            reject(returnJson);
            return returnJson;
          } else {
            _this.dama2.decodeFile(ImageType, filePath, null, 30, function(error, result) {
              if (error) {
                console.log(error);
                returnJson.result = false;
                returnJson.value = "解析图片验证码失败： " + error;
                console.log(returnJson);
                reject(returnJson);
                return returnJson;
              } else {
                _this.dama2.getResult(result.id, function(error, result) {
                  if (error) {
                    returnJson.result = false;
                    returnJson.value = "解析图片验证码失败,接收结果失败： " + error;
                    console.log(returnJson);
                    reject(returnJson);
                    return returnJson;
                  } else {
                    returnJson.result = true;
                    returnJson.value = result.result;
                    console.log(returnJson);
                    resolve(returnJson);
                    return returnJson;
                  }
                });
              }
            });
          }
        });
      }
    });
  }

  // login
}

export default Dama2Service;
