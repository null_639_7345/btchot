import superagent from "superagent";
import isemail from "isemail";
import path from "path";
import fs from "fs";
import arrayUniq from "array-uniq";
import promiseSynchronizer from "promise-synchronizer";

class Btchot {
  constructor() {
    this.cookie = "";
  }
  async analysisCookies(cookies) {
    let cookieArr = [];
    let returnJson= {
      result: false,
      value: ''
    }
    cookies = arrayUniq(cookies);
    return new Promise((resolve, reject) => {
      cookies.forEach(cookie => {
        cookieArr.push(cookie.split(';')[0])
      });
      let cookieStr = cookieArr.join(';');
      if('' == cookieStr){
        returnJson.result = false;
        returnJson.value = 'cookie 解释失败，cookie也许为空'
        resolve(returnJson);
        return returnJson;
      }else{
        returnJson.result = true;
        returnJson.value = cookieStr
        resolve(returnJson);
        return returnJson;
      }
      
    })
    
  }

  async setCookie() {
    let url = `https://www.btchot.com`;
    let _this = this;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise((resolve, reject) => {
      superagent
        .get(url)
        .end(async (err, res) => {
          if (err) {
            returnJson.result = false;
            returnJson.value = "请求cookie失败：" + err;
            reject(returnJson);
            return returnJson;
          } else {
            let tempCookie = await promiseSynchronizer(
              _this.analysisCookies(res.header['set-cookie'])
            );
            if(!tempCookie.result){
              console.log(tempCookie.value);
            }else{
              _this.cookie = tempCookie.value;
            }
            console.log('setCookie cookie: ',_this.cookie);
            returnJson.result = true;
            returnJson.value = _this.analysisCookies(res.header['set-cookie']);
            resolve(returnJson);
            return returnJson;
          }
        });
    });
  }

  /**
   *
   * @param {*} moble 
   * return  { info: '不存在', status: 1, url: '' }
   * { info: '手机号码已存在', status: '0', url: '' }
   * { info: '国家编号错误', status: '0', url: '' }
   */
  async checkmoble(moble) {
    let url = `https://www.btchot.com/reg/check_moble.html`;
    let _this = this;
    let regNum = /^[0-9]\d*$/;

    let returnJson = {
      result: false,
      value: ""
    };
    if(_this.cookie === ""){
      await _this.setCookie()
    }
    console.log('checkmoble cookie: ',_this.cookie);     
    return new Promise((resolve, reject) => {
      if (moble.length !== 11 || !regNum.test(moble)) {
        returnJson.result = false;
        returnJson.value = "验证号码失败，号码的格式不正确";
        reject(returnJson);
        return returnJson;
      } else {
        let JsonDate = {
          'moble': moble,
          'mobles': '+86'
        }
        superagent
          .post(url)
          .send(JsonDate) // sends a JSON post body
          .set('content-type', 'application/x-www-form-urlencoded; charset=UTF-8')
          .set('Cookie', _this.cookie)
          .end(async (err, res) => {
            if (err) {
              returnJson.result = false;
              returnJson.value = "验证号码失败：" + err;
              reject(returnJson);
              return returnJson;
            } else {
              let tempCookie = await promiseSynchronizer(
                _this.analysisCookies(res.header['set-cookie'])
              );
              if(!tempCookie.result){
                console.log(tempCookie.value);
              }else{
                _this.cookie = tempCookie.value;
              }
              returnJson.result = true;
              returnJson.value = res.body;
              resolve(returnJson);
              return returnJson;
            }
          });
      }
      
    });
  }


  /**
   * 获取图片验证码
      https://www.btchot.com/ajax/verify.html?t=0.14478377591792269
   * @param {*} moble 
   */
  async verify() {
    let url = `https://www.btchot.com/ajax/verify.html`;
    let _this = this;

    let returnJson = {
      result: false,
      value: ""
    };
    if(_this.cookie === ""){
      await _this.setCookie()
    }
    console.log('verify cookie: ',_this.cookie);    
    return new Promise((resolve, reject) => {
      let t = Math.random();
      superagent
        .get(url)
        .query({ t: t})
        .set('content-type', 'application/x-www-form-urlencoded; charset=UTF-8')
        .set('Cookie', _this.cookie)
        .end(async (err, res) => {
          if (err) {
            returnJson.result = false;
            returnJson.value = "图片验证码获取失败：" + err;
            reject(returnJson);
            return returnJson;
          } else {
            let tempCookie = await promiseSynchronizer(
              _this.analysisCookies(res.header['set-cookie'])
            );
            if(!tempCookie.result){
              console.log(tempCookie.value);
            }else{
              _this.cookie = tempCookie.value;
            }
            returnJson.result = true;
            returnJson.value = res.body;
            fs.writeFileSync(path.join(__dirname,'../temp/verify/',t+'.png'), res.body)
            returnJson.value = path.join(__dirname,'../temp/verify/',t+'.png');
            resolve(returnJson);
            return returnJson;
          }
        });
    })
  }


  /**
   * 请求短信验证码
   * @param {*} moble 
   * @param {*} verify 
   */
  async mobleReg(moble,verify) {
    let url = `https://www.btchot.com/verify/moble_reg.html`;
    let _this = this;
    let regNum = /^[0-9]\d*$/;
    let regVerify = /\w\w\w\w/;
    let returnJson = {
      result: false,
      value: ""
    };
    if(_this.cookie === ""){
      await _this.setCookie()
    }
    console.log('mobleReg cookie: ',_this.cookie);    
    return new Promise((resolve, reject) => {
      if (moble.length !== 11 || !regNum.test(moble)) {
        returnJson.result = false;
        returnJson.value = "请求短信验证码失败，号码的格式不正确";
        reject(returnJson);
        return returnJson;
      } else {
        if(!regVerify.test(verify)){
          returnJson.result = false;
          returnJson.value = "请求短信验证码失败，图片验证码的解析结果的格式不正确";
          reject(returnJson);
          return returnJson;
        }else{
          let JsonDate = {
            'moble': moble,
            'mobles': '+86',
            verify: verify,
            type: 'sms'
          }
          superagent
            .post(url)
            .send(JsonDate) // sends a JSON post body
            .set('content-type', 'application/x-www-form-urlencoded; charset=UTF-8')
            .set('Cookie', _this.cookie)
            .end(async (err, res) => {
              if (err) {
                returnJson.result = false;
                returnJson.value = "请求短信验证码失败：" + err;
                reject(returnJson);
                return returnJson;
              } else {
                let tempCookie = await promiseSynchronizer(
                  _this.analysisCookies(res.header['set-cookie'])
                );
                if(!tempCookie.result){
                  console.log(tempCookie.value);
                }else{
                  _this.cookie = tempCookie.value;
                }
                returnJson.result = true;
                returnJson.value = res.body;
                resolve(returnJson);
                return returnJson;
              }
            });
        }
      }
    });
  }

  /**
   * 注册信息
   * @param {*} moble 
   * @param {*} mobleVerify 收到的短信验证码
   * @param {*} password 
   * @param {*} invit 推荐码，邀请码
   */
  async regUp(moble,mobleVerify,password,invit) {
    let url = `https://www.btchot.com/reg/reg_up.html`;
    let _this = this;
    let regNum = /^[0-9]\d*$/;
    let regVerify = /\w\w\w\w/;
    let returnJson = {
      result: false,
      value: ""
    };
    if(_this.cookie === ""){
      await _this.setCookie()
    }
    console.log('regUp cookie: ',_this.cookie);    
    return new Promise((resolve, reject) => {
      if (moble.length !== 11 || !regNum.test(moble)) {
        returnJson.result = false;
        returnJson.value = "注册信息失败，号码的格式不正确";
        reject(returnJson);
        return returnJson;
      } else {
        if(mobleVerify.length !== 6 || !regNum.test(mobleVerify)){
          returnJson.result = false;
          returnJson.value = "注册信息失败，短息验证码结果的格式不正确";
          reject(returnJson);
          return returnJson;
        }else{
          let JsonDate = {
            'moble': moble,
            'mobles': '+86',
            'moble_verify': mobleVerify,
            'password': password,
            'invit': invit
          }
          superagent
            .post(url)
            .send(JsonDate) // sends a JSON post body
            .set('content-type', 'application/x-www-form-urlencoded; charset=UTF-8')
            .set('Cookie', _this.cookie)
            .end(async (err, res) => {
              if (err) {
                returnJson.result = false;
                returnJson.value = "注册信息失败：" + err;
                reject(returnJson);
                return returnJson;
              } else {
                let tempCookie = await promiseSynchronizer(
                  _this.analysisCookies(res.header['set-cookie'])
                );
                if(!tempCookie.result){
                  console.log(tempCookie.value);
                }else{
                  _this.cookie = tempCookie.value;
                }
                returnJson.result = true;
                returnJson.value = res.body;
                resolve(returnJson);
                return returnJson;
              }
            });
        }
      }
    });
  }

  /**
   * 完善信息
   * @param {*} username 8-12位大小写字母
   * @param {*} email 
   * @param {*} paypassword 6位数字
   */
  async paypasswordUp(username,email,paypassword) {
    let url = `https://www.btchot.com/reg/paypassword_up.html`;
    let _this = this;
    let returnJson = {
      result: false,
      value: ""
    };
    if(_this.cookie === ""){
      await _this.setCookie()
    }    
    console.log('paypasswordUp cookie: ',_this.cookie);
    return new Promise((resolve, reject) => {
      if (!isemail.validate(email)) {
        returnJson.result = false;
        returnJson.value = "完善信息失败，邮箱的格式不正确";
        reject(returnJson);
        return returnJson;
      } else {
        if(username.length < 6){
          returnJson.result = false;
          returnJson.value = "完善信息失败,用户名格式不正确";
          reject(returnJson);
          return returnJson;
        }else{
          let JsonDate = {
            'username': username,
            'email': email,
            'paypassword': paypassword
          }
          superagent
            .post(url)
            .send(JsonDate) // sends a JSON post body
            .set('content-type', 'application/x-www-form-urlencoded; charset=UTF-8')
            .set('Cookie', _this.cookie)
            .end(async (err, res) => {
              if (err) {
                returnJson.result = false;
                returnJson.value = "完善信息失败： " + err;
                reject(returnJson);
                return returnJson;
              } else {
                let tempCookie = await promiseSynchronizer(
                  _this.analysisCookies(res.header['set-cookie'])
                );
                if(!tempCookie.result){
                  console.log(tempCookie.value);
                }else{
                  _this.cookie = tempCookie.value;
                }
                returnJson.result = true;
                returnJson.value = res.body;
                resolve(returnJson);
                return returnJson;
              }
            });
        }
      }
    });
  }

}

export default Btchot;
