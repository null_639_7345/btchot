import models from '../models';


class btchotService {
  /**
   * 根据粉丝id查找粉丝信息，按时间排序
   * @param userId
   */
  async getbtchotByUserId(userId) {
    let returnjson = { results: false, value: { err: { errCode: 94 } } }; //94  参数不合理
    let data = null;
    let json = {};
    var params = { userId: userId };
    if (params) {
      json.where = params;
    }
    await models.btchot.findAll(json).then(res => {
      if (res.length > 0) {
        data = res.map(r => {
          return r.dataValues;
        });
        returnjson.results = true;
        returnjson.value = data;
      }
    });
    return returnjson;
  }

  /**
   * 获取所有的btchot的信息
   */
  async getAllbtchot() {
    let returnjson = { results: false, value: { err: { errCode: 94 } } }; //94  参数不合理
    let querystr =
      'SELECT * FROM heweiwechat.btchot order by updatedAt desc';
    await models.sequelize
      .query(querystr, { type: models.sequelize.QueryTypes.SELECT })
      .then(res => {
        returnjson.results = true;
        returnjson.value = res;
      });
    return returnjson;
  }

  /**
   * 创建btchot
   * 
   * @param {any} json 
   * @returns 
   */
  async createbtchot(json) {
    let returnjson = { results: false, value: { err: { errCode: 97 } } }; //97 Required parameter missing.
    await models.btchot.create(json).then(res => {
      returnjson.results = true;
      console.log('!!!!!',res);
      debugger;
      if(res[0].dataValues !== undefined){
        returnjson.value = res[0].dataValues;
      }else{
        returnjson.value = res;
      }
    });
    return returnjson;
  }

  

  
}
export default btchotService;
