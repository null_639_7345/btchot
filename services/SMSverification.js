import superagent from "superagent";

class SMSverification {
  constructor(uName, pWord, Developer) {
    this.uName = uName;
    this.pWord = pWord;
    this.Developer = Developer;
    this.token = "";
  }

  /**
   * 登录接口，也是设置token的接口，
   * token10分钟失效
   */
  async setToken() {
    let loginUrl = `http://xapi.yika66.com/User/login`;
    let _this = this;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise((resolve, reject) => {
      if (_this.uName === "" || _this.pWord === "" || _this.Developer === "") {
        returnJson.result = false;
        returnJson.value = "请求token失败，参数不正确";
        reject(returnJson);
        return returnJson;
      } else {
        let sendData = {
          uName: _this.uName,
          pWord: _this.pWord,
          Developer: _this.Developer,
          code: "utf8"
        };
        superagent
          .get(loginUrl)
          .query(sendData)
          .end((err, res) => {
            if (err) {
              returnJson.result = false;
              returnJson.value = "请求token失败：" + err;
              reject(returnJson);
              return returnJson;
            } else {
              _this.token = res.text.split("&")[0];
              returnJson.result = true;
              returnJson.value = res.text.split("&");
              resolve(returnJson);
              return returnJson;
            }

            // Do something
          });
      }
    });
  }

  /**
   *
   */
  getToken() {
    let returnJson = {
      result: false,
      value: ""
    };
    if (this.token === "") {
      returnJson.result = false;
      returnJson.value = "token 为空";
    } else {
      returnJson.result = true;
      returnJson.value = this.token;
    }
    return returnJson;
  }

  /**
   * 获取号码 GET方式GB2312调用实例- http://xapi.yika66.com/User/getPhone?ItemId=项目ID&token=登陆token
   * @param {*} Json 
   * 
  参数名	必传	缺省值	描述
  token	    Y		登录token
  ItemId	Y		项目代码
  Phone	    N		指定手机号
  Count	    N	1	获取数量 [不填默认1个]
  Area	    N		区域 [不填则 随机]
  PhoneType	N	0	运营商 [不填为 0] 0 [随机] 1 [移动] 2 [联通] 3 [电信]
  code	    N	utf8	转UTF8编码
   */
  async getPhone(Json) {
    let getPhoneUrl = `http://xapi.yika66.com/User/getPhone`;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise((resolve, reject) => {
      if (Json.token === "" || Json.ItemId === "") {
        returnJson.result = false;
        returnJson.value = "获取号码失败，参数不正确";
        reject(returnJson);
        return returnJson;
      } else {
        superagent
          .get(getPhoneUrl)
          .query(Json)
          .end((err, res) => {
            if (err) {
              returnJson.result = false;
              returnJson.value = "获取号码失败：" + err;
              reject(returnJson);
              return returnJson;
            } else {
              let regNum = /^[0-9]\d*$/;
              //False:Session 过期
              let moble = res.text.split(";")[0];
              if (moble.length !== 11 || !regNum.test(moble)) {
                returnJson.result = false;
                returnJson.value = "获取号码失败：" + moble;
                reject(returnJson);
                return returnJson;
              } else {
                returnJson.result = true;
                returnJson.value = moble;
                resolve(returnJson);
                return returnJson;
              }
              
            }
            // Do something
          });
      }
    });
  }

  /**
   * \
6、获取消息
GET - http://xapi.yika66.com/User/getMessage?token=登陆token&ItemId=项目ID&Phone=获取的号码

6.1、请求参数
参数名	必传	缺省值	描述
token	Y		登录token
ItemId	Y		项目ID
Phone	Y		获取的号码
code	N	utf8	转UTF8编码

   * @param {*} Json 
   */
  async getMessage(Json) {
    let getMessageUrl = `http://xapi.yika66.com/User/getMessage`;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise((resolve, reject) => {
      if (Json.token === "" || Json.ItemId === "" || Json.Phone === "") {
        returnJson.result = false;
        returnJson.value = "获取消息失败，参数不正确";
        reject(returnJson);
        return returnJson;
      } else {
        superagent
          .get(getMessageUrl)
          .query(Json)
          .end((err, res) => {
            if (err) {
              returnJson.result = false;
              returnJson.value = "获取消息失败：" + err;
              reject(returnJson);
              return returnJson;
            } else {
              returnJson.result = true;
              //False:Session 过期
              returnJson.value = res.text.split("&");
              resolve(returnJson);
              return returnJson;
            }
            // Do something
          });
      }
    });
  }

  /**
   * 提取getMessage返回结果中的验证码
   * return result 状态 value 结果
   */
  getVerification(Message) {
    let returnJson = {
      result: false,
      value: ""
    };
    if (Message[0] !== "MSG") {
      returnJson.result = false;
      returnJson.value = "Message 中不包含验证码";
    } else {
      let reg = /验证码是(\d+)\[/g;
      Message[3].match(reg);
      let Verification = RegExp.$1;
      if (Verification === "") {
        returnJson.result = false;
        returnJson.value = "验证码提取失败";
      } else {
        let regNum = /^[0-9]\d*$/;
        if (!regNum.test(Verification)) {
          returnJson.result = false;
          returnJson.value = "验证码格式错误";
        } else {
          returnJson.result = true;
          returnJson.value = Verification;
        }
      }
    }
    return returnJson;
  }

  /**
   * http://xapi.yika66.com/User/releasePhone?token=登陆token&phoneList=phone-itemId;phone-itemId;

号码列表格式: phone-itemId;phone-itemId; 其中phone为号码，itemId为项目ID, 请注意后面的分号需要加上
   * @param {*} Json 
   */
  async releasePhone(Json) {
    let _this = this;
    let releasePhoneUrl = `http://xapi.yika66.com/User/releasePhone`;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise((resolve, reject) => {
      if (Json.token === "" || Json.phoneList === "") {
        returnJson.result = false;
        returnJson.value = "释放号码失败，参数不正确";
        reject(returnJson);
        return returnJson;
      } else {
        superagent
          .get(releasePhoneUrl)
          .query(Json)
          .end(async (err, res) => {
            if (err) {
              returnJson.result = false;
              returnJson.value = "释放号码失败： " + err;
              reject(returnJson);
              return returnJson;
            } else {
              returnJson.result = true;
              //False:Session 过期
              returnJson.value = res.body;
              let exitJson = {
                token: Json.token
              }
              await _this.exit(exitJson)
              resolve(returnJson);
              return returnJson;
            }
            // Do something
          });
      }
    });
  }
  
  /**
   * GET - http://xapi.yika66.com/User/ReleaseAllPhone?token=登陆token
   * @param {*} Json 
   */
  async releaseAllPhone(Json) {
    let _this = this;
    let releaseAllPhoneUrl = `http://xapi.yika66.com/User/ReleaseAllPhone`;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise((resolve, reject) => {
      if (Json.token === "") {
        returnJson.result = false;
        returnJson.value = "释放全部号码失败，参数不正确";
        reject(returnJson);
        return returnJson;
      } else {
        superagent
          .get(releaseAllPhoneUrl)
          .query(Json)
          .end(async (err, res) => {
            if (err) {
              returnJson.result = false;
              returnJson.value = "释放全部号码失败： " + err;
              reject(returnJson);
              return returnJson;
            } else {
              returnJson.result = true;
              //False:Session 过期
              returnJson.value = res.body;
              let exitJson = {
                token: Json.token
              }
              await _this.exit(exitJson)
              resolve(returnJson);
              
              return returnJson;
            }
            // Do something
          });
      }
    });
  }

  async exit(Json) {
    let exitUrl = `http://xapi.yika66.com/User/exit`;
    let returnJson = {
      result: false,
      value: ""
    };
    return new Promise((resolve, reject) => {
      if (Json.token === "") {
        returnJson.result = false;
        returnJson.value = "退出失败，参数不正确";
        reject(returnJson);
        return returnJson;
      } else {
        superagent
          .get(exitUrl)
          .query(Json)
          .end((err, res) => {
            if (err) {
              returnJson.result = false;
              returnJson.value = "退出失败： " + err;
              reject(returnJson);
              return returnJson;
            } else {
              returnJson.result = true;
              //False:Session 过期
              returnJson.value = res.body;
              resolve(returnJson);
              return returnJson;
            }
            // Do something
          });
      }
    });
  }

}

export default SMSverification;
