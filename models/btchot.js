'use strict';
/**
 * @param {*} sequelize 
 * @param {*} DataTypes 
 */
module.exports = function(sequelize, DataTypes) {
  var btchot = sequelize.define('btchot', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    Phone: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    invit: DataTypes.STRING,
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: DataTypes.STRING,
    paypassword: {
      type: DataTypes.STRING,
      allowNull: false
    },
    createdAt:{
      type: DataTypes.DATE
    }, 
    updatedAt:{
      type: DataTypes.DATE
    } 
  }, {
    classMethods: {
      associate: function (models) {
      }
    }
  });
  return btchot;
};